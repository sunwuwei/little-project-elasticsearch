package com.ruyuan.little.project.elasticsearch.biz.common.dto;

import com.ruyuan.little.project.common.dto.TableData;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/
@Getter
@Setter
@ToString
public class FuzzyQueryData<T> extends TableData<T> {

    /**
     * 查询内容
     */
    private String queryContent;

    /**
     * 模糊查询之后的内容
     */
    private String fuzzyContent;

}
